import React, { Component } from 'react';
import './App.css';

import GameTitle from './components/GameTitle/GameTitle';
import GameMenu from './components/GameMenu/GameMenu';
import GameMap from './components/GameMap/GameMap';
import GameCore from './services/GameCore';

const defGameSize = 5;
export default class App extends Component {

  Game = new GameCore(defGameSize);
  state = this.Game.Status();

  updateState = () => {
    this.setState(this.Game.Status());
  }

  /**
   * Click on 'Size' button handler
   */
  onSizeClick = () => {
    let newSize = this.state.size + 1;
    if (newSize > this.Game.maxSize)
      newSize = this.Game.minSize;
    this.Game.Restart(newSize);
    this.updateState();
  }

  /**
   * Click on 'New' button handler
   */
  onNewClick = () => {
    this.Game.Restart(this.state.size);
    this.updateState();
  }

  /**
   * Click on 'Undo' button handler
   */
  onUndoClick = () => {
    this.Game.Undo();
    this.updateState();
  }

  /**
   * Click on some map cell handler
   */
  onMapClick = (x, y) => {
    if (!this.state.gameOver) {
      this.Game.Click(x, y);
      this.updateState();
    }
  }

  render() {
    return (
      <div className="App">
        <GameTitle />
        <GameMenu 
          { ...this.state }
          OnSizeClick = { this.onSizeClick }
          OnNewClick = { this.onNewClick }
          OnUndoClick = { this.onUndoClick }
        />
        <GameMap 
          mapData = { this.state.data } 
          OnMapClick = { this.onMapClick }
        />
      </div>
    );
  }
}
