export default class GameCore {
    /**
     * Constructor
     * @param {number} size game map size
     */
    constructor(size) {
        this.minSize = 4;
        this.maxSize = 10;
        this.maxNumber = 12;
        if (size === undefined)
            size = 6;
        this.Restart(size);
    }
    /**
     *  Create new game
     * @param {number} size game map size
     */
    Restart = (size) => {
        let mSize = (size < this.minSize) ? this.minSize : size;
        mSize = (mSize > this.maxSize) ? this.maxSize : mSize;
        this.nWidth = 8;
        this.map = [];
        this.mapCopy = [];
        this.size = mSize;
        this.curMin = 1;
        this.curMax = 5;
        this.gameOver = false;
        this.score = 0;
        this.oldScore = 0;
        this.createMap();
    }

    /**
     * Create new game map with size = this.size
     */
    createMap() {
        for (let y = 0; y < this.size; y++) {
            this.map[y] = [];
            this.mapCopy[y] = [];
            for (let x = 0; x < this.size; x++) { this.map[y][x] = this.getRandom(); }
        }
    }

    /**
     *  Generate random number in range [curMin..curMax]
     * @returns {number} random number
     */
    getRandom() {
        return Math.floor(Math.random() * (this.curMax - this.curMin) + this.curMin);
    }

    /**
     *  Get cell value from game map
     * @param {number} x X map coordinate
     * @param {number} y Y map coordinate
     * @returns {number} cell value from map[x,y], or 0
     */
    getCell(x, y) {
        return (x >= 0 && x < this.size && y >= 0 && y < this.size) ? this.map[y][x] : 0;
    }

    /**
     *  Set cell value into map
     * @param {number} x X map coordinate
     * @param {number} y Y map coordinate
     * @param {number} cell cell value
     */
    setCell(x, y, cell) {
        if (x >= 0 && x < this.size && y >= 0 && y < this.size) { this.map[y][x] = cell; }
    }

    /**
     *  Increase cell value in map, if possible
     * @param {number} x X map coordinate
     * @param {number} y Y map coordinate
     */
    incCell(x, y) {
        if (x >= 0 && x < this.size && y >= 0 && y < this.size && this.getCell(x, y) < this.maxNumber) { this.map[y][x]++; }
    }

    /**
     * Return game status in JSON:
     * {
     *  id: 'map',
     *  data: map,              // array
     *  score: score,           // number
     *  gameOver: gameOver      // boolean
     * }
     */
    Status() {
        return {
            data: this.map,
            score: this.score,
            size: this.size,
            gameOver: this.gameOver,
        }
    }

    /**
     *  Search around the specified cell with a value equal to the specified value
     * @param {number} x X coordinate of specified cell
     * @param {number} y Y coordinate of specified cell
     * @param {number} cell specified value
     * @returns {number} value with bits sets in the required fields
     * @returns bit 0 : equal value found at left cell
     * @returns bit 1 : equal value found at right cell
     * @returns bit 2 : equal value found at up cell
     * @returns bit 3 : equal value found at down cell
     */
    searchAround(x, y, cell) {
        let res = 0;
        if (cell === this.getCell(x - 1, y)) { res |= 0x01; }
        if (cell === this.getCell(x + 1, y)) { res |= 0x02; }
        if (cell === this.getCell(x, y - 1)) { res |= 0x04; }
        if (cell === this.getCell(x, y + 1)) { res |= 0x08; }
        return res;
    }

    /**
     *  Shift map column down
     * @param {number} x X coordinate
     * @param {number} y Y coordinate
     */
    shiftColumn(x, y) {
        for (let i = y; i > 0; i--) { this.setCell(x, i, this.getCell(x, i - 1)); }
        this.setCell(x, 0, this.getRandom());
    }

    /**
     *  Delete blank cells in map and shift column down
     */
    clearBlankCells() {
        for (let x = 0; x < this.size; x++) {
            for (let y = 0; y < this.size; y++) {
                if (this.getCell(x, y) === 0) { this.shiftColumn(x, y); }
            }
        }
    }

    /**
     *  Save game status for possible future undo
     */
    saveGameStatus() {
        this.oldScore = this.score;
        this.map.forEach((item, i) => {
            this.mapCopy[i] = item.slice();
        });
    }

    /**
     *  Restore game status for undo
     */
    restoreGameStatus() {
        this.score = this.oldScore;
        this.mapCopy.forEach((item, i) => {
            this.map[i] = item.slice();
        });
    }

    /**
     *  Calculate game score
     * @param {number} score
     */
    addScore(score) {
        this.score += score;
    }

    /**
     *  Looking for the whole cell chain started from specified, and combine it if possible,
     *  calculate game score and define range of cell numbers
     * @param {number} x X coordinate of specified cell
     * @param {number} y Y coordinate of specified cell
     */
    combineCells(x, y) {
        const qu = [];
        const cellN = this.getCell(x, y);
        if (this.searchAround(x, y, cellN) === 0) { return; }
        this.incCell(x, y);
        qu.push(x, y);
        do {
            const cy = qu.pop();
            const cx = qu.pop();
            const findRes = this.searchAround(cx, cy, cellN);
            if (findRes & 0x01) {
                qu.push(cx - 1, cy);
                this.setCell(cx - 1, cy, 0);
                this.addScore(cellN);
            }
            if (findRes & 0x02) {
                qu.push(cx + 1, cy);
                this.setCell(cx + 1, cy, 0);
                this.addScore(cellN);
            }
            if (findRes & 0x04) {
                qu.push(cx, cy - 1);
                this.setCell(cx, cy - 1, 0);
                this.addScore(cellN);
            }
            if (findRes & 0x08) {
                qu.push(cx, cy + 1);
                this.setCell(cx, cy + 1, 0);
                this.addScore(cellN);
            }
        } while (qu.length);
        if (cellN === this.maxSize) { this.setCell(x, y, cellN); }
        if (this.curMax < (cellN + 1) && this.curMax < this.maxSize) { this.curMax++; }
        if ((this.curMax - this.curMin) > this.nWidth) { this.curMin++; }
    }

    /**
     *  Method, called when clicked on a cell
     * @param {number} x X coordinate cell
     * @param {number} y Y coordinate cell
     */
    Click(x, y) {
        if (x >= 0 && y >= 0 && x < this.size && y < this.size && !this.gameOver) {
            this.saveGameStatus();
            this.combineCells(x, y);
            this.clearBlankCells();
            this.gameOver = this.isGameOver();
        }
    }

    /**
     *  Check on the end of the game
     * @returns {boolean} false, if game is not over
     * @returns {boolean} true,  if game is over
     */
    isGameOver() {
        for (let x = 0; x < this.size; x++) {
            for (let y = 0; y < this.size; y++) {
                if (this.searchAround(x, y, this.getCell(x, y))) return false;
            }
        }
        return true;
    }

    /**
     *  Return game to previous state
     */
    Undo() {
        if (this.mapCopy[0][0] != null) { this.restoreGameStatus(); }
    }
}