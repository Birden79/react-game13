import React from "react"
import './game-menu.css'

export default class GameMenu extends React.Component  {
	render() {
        const sizeText = `Size: ${this.props.size}`;
        const infoText = this.props.gameOver ? 'Game is over' : `Score: ${this.props.score}`;
        return (
            
            <div className="game-menu">
                <div className="buttons">
                    <div className="button" onClick={ this.props.OnSizeClick }>{sizeText}</div>
                    <div className="button" onClick={ this.props.OnNewClick  }>New game</div>
                    <div className="button" onClick={ this.props.OnUndoClick }>Undo</div>
                </div>
                <div className="score">{ infoText } </div>
            </div>
    
        )
    }
}


