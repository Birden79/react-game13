import React from 'react';
import './game-title.css';

const GameTitle = () => {
  const rulesText = `At each moment of the game, the player can choose one of the tiles from a group of
     ajacent tiles containing the same numer. By selecting one of the tiles from a group of connected tiles,
     selected tile is replaced by a new tile with increased number, and all other tiles of the selected group are removed.
     After it, to fill up the gaps created by the removed tiles, all tiles "fall down" in vertical direction as far as possible.
     Gaps, remaining at the upper part are filled by random new tiles. The goal of the fame is to obtain a tile with the 
     largest possible number (13 or more). The game ends if no choices are left.`;
  return (
    <div className="game-title">
        <h2>Game rules:</h2>
        <p className="rules-text">{ rulesText }</p>
    </div>
  );
}
export default GameTitle;