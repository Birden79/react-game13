import React from "react"
import './game-map.css'

export default class GameMap extends React.Component {
	
	render() {
		const mapDiv = this.props.mapData.map( (row, y) => {
			const rowKey = `${y}`;
			const line =  row.map((item, x) => {
				const cellClass = `cell cl-${item}`;
				const cellKey = `${x}${rowKey}`;
				return <td className={ cellClass } key={ cellKey } onClick={ ()=> { this.props.OnMapClick(x,y) } } >  { item } </td>
			});
			return <tbody key={ rowKey }><tr>{ line }</tr></tbody>;
		});
		return (
			<table className="game-map"> 
				{ mapDiv }
			</table>
		)
	}
}

